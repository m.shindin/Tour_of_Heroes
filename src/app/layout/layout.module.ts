import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from '../app-routing.module';
import { MessageModule } from '../message/message.module';


@NgModule({
  declarations: [HeaderComponent, HomeComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    MessageModule
  ],
  exports: [HomeComponent]
})
export class LayoutModule { }
